#!/usr/bin/python

# Copyright: (c) 2021, Ross Davies <davies.ross@gmail.com>
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
from __future__ import absolute_import, division, print_function
__metaclass__ = type

DOCUMENTATION = r'''
    name: prism_vm
    plugin_type: module
    short_description: Manages virtual machines in either Nutanix Prism Element or Nutanix Prism Central.
    description:
        - This module can be used to create new virtual machines, manage the power state of virtual machine such as power on, power off, restart etc. Modify various virtual machine components like cpu, memory, network, disk etc. Finally it is also possible to remove a virtual machine along with its associated configuration.
    author:
        - Ross Davies <davies.ross@gmail.com>
    version_added: 0.0.4
    requirements:
        - "python >= 2.7"
        - "ntnx-api >= 1.1.30"
    notes:
        - Check mode is supported.
        - Ensure that the user account used to connect to Prism Element or Prism Central has the appropriate rights to create and manage virtual machines.
        - If the change being requested is disruptive (such as changing virtual CPU) it will fail unless the force parameter is set to True. In this case the virtual machine will be powered off for the change then returned to its original power state.
        - Although VM names do not have on each Nutanix cluster, this module assumes that within each individual cluster there is only one instance of each virtual machine name.
    options:
        prism:
            description:
                - The IP address or hostname for either Nutanix Prism Element or Nutanix Prism Central to connect.
            type: str
            required: True
        port:
            description:
                - The TCP port for either Nutanix Prism Element or Nutanix Prism Central to connect.
            type: int
            required: False
        username:
            description:
                - The username for either Nutanix Prism Element or Nutanix Prism Central for authentication.
            type: str
            required: False
        password:
            description:
                - The password for either Nutanix Prism Element or Nutanix Prism Central for authentication.
            type: str
            required: True
        validate_certs:
            type: str
            choices: ['yes', 'no']
            default: 'yes'
        state:
            description:
                - Specify the state that the virtual machine should be in.
                - If C(state) is set to C(present) and the virtual machine exists its configuration will be updated to match that provided.
                - If C(state) is set to C(present) and the virtual machine does not exist it will be created. In this mode the power state will be set to off. If a VM with the same name already exists the VM creation will fail.
                - If C(state) is set to C(absent) and the virtual machine exists it will be deleted. Any snapshots of this VM will be deleted. Any attached volume groups will be detached.
                - If C(state) is set to one of the following C(on), C(off), C(powercycle) and the virtual machine does not exists, it will be created with the given parameters and the power state set.
                - If C(state) is set to C(on) and the virtual machine exists the power state will be powered on.
                - If C(state) is set to C(off) and the virtual machine exists the power state will be powered off.
                - If C(state) is set to C(reset) and the virtual machine exists the VM will be hard reset.
                - If C(state) is set to C(powercycle) and the virtual machine exists the VM will be powered off then back on.
                - If C(state) is set to C(guest-reboot) and the virtual machine exists the VM will be soft rebooted.
                - If C(state) is set to C(guest-shutdown) and the virtual machine exists the VM will be soft shutdown.
                - If C(state) is set to C(resume) and the virtual machine exists the VM will be resumed.
                - If C(state) is set to C(suspend) and the virtual machine exists the VM will be suspended.
            default: 'present'
            type: str
            choices: ['absent', 'present', 'on', 'off', 'guest-reboot', 'guest-shutdown', 'reset', 'powercycle', 'resume', 'suspend']
            required: True
        cluster_name:
            description:
                - If connecting to an instance of Prism Central this field is mandatory.
                - Set to the name of a Prism Element cluster where the virtual machine is to be created, modified or have its power state changed.
            type: str
            required: False
        name:
            description:
                - The name of the virtual machine to work with.
                - Virtual machine names within Prism are not necessarily unique although it would be a best practice to ensure uniqueness within each Nutanix cluster. For the purposes of this module it is assumed that the virtual machine names have been kept unique and that if duplicate occur they are in different clusters (ie. VMs with the same name under management by Prism Central are ok as long as they are located in seperate clusters).
                - This parameter is required.
            type: str
            required: True
        cores:
            description:
                - The number of virtual cores / virtual CPUs to set on the virtual machine.
            type: int
            required: False
        sockets:
            description:
                - The number of virtual sockets to set on the virtual machine.
            type: int
            required: True
        memory_gb:
            description:
                - The amount of virtual memory in GB to set on the virtual machine.
            type: int
            required: True
        cdrom:
            description: 
                - Whether to add a cdrom drive to the virtual machine.
            type: bool
            choices: [True, False]
            default: True
        disks:
            description:
                - A list of disks to add in order.
                - Reducing disk size below what is already provisioned is not supported.
                - Removing the virtual machines existing disks is not supported.
                - Only one of C(disks.storage_container_name) C(disks.image_name) or C(disks.volume_group_name) can be provided per-disk.
                - Currently only applies to VM creation tasks.
            type: list
            elements: dict
            required: False
            suboptions:
                bus:
                    description:
                        - The type of bus to attach the virtual machine disk onto.
                    type: str
                    default: 'scsi'
                    choices: ['scsi', 'ide', 'pci', 'sata', 'spapr', 'nvme']
                size_gb:
                    description:
                        - Disk size in GB.
                        - Only applicable when creating a new virtual disk or cloning from an image.
                        - Has to be larger than the size of the image being used.
                    required: True
                storage_container_name:
                    description:
                        - The name for the container on which to create this virtual disk.
                        - Mutually exclusive with C(image_name) and C(volume_group_name)
                    type: str
                image_name:
                    description:
                        - The name for the image in the image service to clone for this virtual disk.
                        - The image has to be present.
                        - Mutually exclusive with C(storage_container_name) and C(volume_group_name)
                    type: str
                volume_group_name:
                    description:
                        - The name for the volume group to attach to this virtual machine.
                        - The volume group has to be present.
                        - Be wary of the default OS operation when detecting an iSCSI attached volume. Attaching a shared drive already in use elsewhere can cause data availability issues.
                        - Mutually exclusive with C(storage_container_name) and C(image_name)
                    type: str
                flash_mode:
                    description:
                        - Whether to enable flash mode on this virtual disk
                    type: bool
                    choices: [True, False]
                    default: False
        nics:
            description:
                - A list of network interfaces to add in order.
                - Currently only applies to VM creation tasks.
            type: list
            elements: dict
            suboptions:
                network_name:
                    description:
                        - The name of the AHV network to connect this network interface onto.
                    type: str
                    required: True
                adaptor_type:
                    description:
                        - The type of the network adaptor being added.
                        - Not required for AHV.
                    type: str
                    choices: ['e1000', 'e1000e', 'pcnet32', 'vmxnet', 'vmxnet2', 'vmxnet3']
                    default: 'e1000'
                connect:
                    description:
                        - Whether the network interface should be in a connected state or not.
                        - Setting this to C(True) will put the network interface into a connected state.
                        - Setting this to C(False) will put the network interface into a disconnected state.
                    type: bool
                    choices: [True, False]
                    default: True
                mac_address:
                    description:
                        - A custom MAC address for this network adaptor.
                    type: str
                ipam:
                    description:
                        - Whether to use AHV IPAM to assign an ip address on this network connection.
                    type: bool
                    choices: [True, False]
                    default: False
                requested_ip_address:
                    description:
                        - Defines the IP address to assign to this network adaptor rather than assigning a random IP address from the networks IP pool. 
                        - Requires C(nic.ipam) to be set to True.
                    type: str
        gpus:
            description:
                - A list of GPUs to add in order.
                - Currently only applies to VM creation tasks.
            type: list
            elements: dict
            suboptions:
                device_id:
                    description:
                        - TBD.
                    type: int
                    required: True
                gpu_type:
                    description:
                        - The type of GPU to add to the virtual machine.
                    choices: ['pass_through_graphics', ]
                    default: 'pass_through_graphics'
                gpu_vendor:
                    description:
                        - The GPU vendor.
                    choices: ['nvidia', ]
                    default: 'nvidia'
        serial_ports:
            description:
                - A list of serial port interfaces to add in order.
                - Currently only applies to VM creation tasks.
            type: list
            elements: dict
            suboptions:
                port_index:
                    description:
                        - TBD
                    type: int
                    required: True
                port_type:
                    description:
                        - TBD
                    choices: ['null', 'server']
                    default: 'null'
        sysprep:
            description:
                - The sysprep content in XML format.
                - Should only be provided for windows systems being deployed.
                - Only applies to VM creation tasks.
                - Is mutually exclusive with C(cloudinit)
        cloudinit:
            description:
                - The cloudinit content in text format.
                - Should only be provided for linux systems being deployed.
                - Only applies to VM creation tasks.
                - Is mutually exclusive with C(sysprep)
        machine_type:
            description:
                - The architecture for the cluster hosting the virtual machine.
                - C(pc) is to be used for x86 clusters.
                - C(pseries) is to be used for IBM P-Series clusters.
                - C(q35) is to be used for systems running the Intel Q35 embedded systems chipset.
            choices: ['pc', 'pseries', 'q35']
            default: 'pc'
        timezone:
            description:
                - The timezone for the virtual machine.
            choices: ['utc']
            default: 'utc'
        force:
            description: 
                - If the VM exists and its power state does not allow the requested change to be made and this option is set to False then the task will fail.
                - If the VM exists and its power state does not allow the requested change to be made and this option is set to True then the virtual machines power state will be changed to all the request change to be implemented before being returned to its original value.
            type: bool
            choices: [True, False]
            default: True
'''

EXAMPLES = r'''
'''

RETURN = r'''
'''

import traceback

try:
    from ntnx_api.client import PrismApi
    from ntnx_api import prism
    HAS_NTNX_API = True
except ImportError:
    NTNX_API_IMP_ERR = traceback.format_exc()
    HAS_NTNX_API = False

from ansible.module_utils.basic import AnsibleModule, missing_required_lib
from ansible_collections.grdavies.nutanix.plugins.module_utils.common import prism_argument_spec


def _get_container(api_client, clusteruuid, name=None, uuid=None):
    """
    Find an existing container by either name or uuid
    Args:
        api_client: Nutanix API Client object
        clusteruuid: The UUID of the cluster to search within
        name: The name of the container to search for
        uuid: The uuid of the container to search for
    Returns: A dict containing the definition of the container
    """
    result = None
    container_obj = prism.StorageContainer(api_client=api_client)

    if name:
        result = container_obj.search_name(name=name, clusteruuid=clusteruuid)
    elif uuid:
        result = container_obj.search_uuid(uuid=uuid, clusteruuid=clusteruuid)

    return result


def _get_image(api_client, clusteruuid, name=None, uuid=None):
    """
    Find an existing image by either name or uuid
    Args:
        api_client: Nutanix API Client object
        clusteruuid: The UUID of the cluster to search within
        name: The name of the image to search for
        uuid: The uuid of either the image of the disk image for the image to search for
    Returns: A dict containing the definition of the image
    """
    result = None
    image_obj = prism.Images(api_client=api_client)

    if name:
        result = image_obj.search_name(name=name, clusteruuid=clusteruuid)
    elif uuid:
        result = image_obj.search_uuid(uuid=uuid, clusteruuid=clusteruuid)

    return result


def _get_volume_group(api_client, clusteruuid, name=None, uuid=None):
    """
    Find an existing volume group by either name or uuid
    Args:
        api_client: Nutanix API Client object
        clusteruuid: The UUID of the cluster to search within
        name: The name of the volume group to search for
        uuid: The uuid of the volume group to search for
    Returns: A dict containing the definition of the volume group
    """
    result = None
    volume_group_obj = prism.StorageVolume(api_client=api_client)

    if name:
        result = volume_group_obj.search_volume_groups_name(name=name, clusteruuid=clusteruuid)
    elif uuid:
        result = volume_group_obj.search_volume_groups_uuid(uuid=uuid, clusteruuid=clusteruuid)

    return result


def _get_network(api_client, clusteruuid, name=None, uuid=None):
    """
    Find an existing network by either name or uuid
    Args:
        api_client: Nutanix API Client object
        clusteruuid: The UUID of the cluster to search within
        name: The name of the network to search for
        uuid: The uuid of the network to search for
    Returns: A dict containing the definition of the network
    """
    result = None
    network_obj = prism.Network(api_client=api_client)

    if name:
        result = network_obj.search_name(name=name, clusteruuid=clusteruuid)
    elif uuid:
        result = network_obj.search_uuid(uuid=uuid, clusteruuid=clusteruuid)

    return result


def get_existing_vm_spec(api_client, clusteruuid, vm):
    """
    Build the specification for an existing VM
    Args:
        api_client: Nutanix API Client object
        clusteruuid: The UUID of the cluster to search within
        vm: The VM dict returned from the ntnx-api library
    Returns: A dict containing the specification for this VMs configuration
    """
    network_obj = prism.Network(api_client=api_client)
    container_obj = prism.StorageContainer(api_client=api_client)
    image_obj = prism.Images(api_client=api_client)
    volume_group_obj = prism.StorageVolume(api_client=api_client)

    if vm.get('machine_type'):
        vm.get('machine_type').lower()
    else:
        vm['machine_type'] = 'pc'

    config = {
        'name': vm.get('name'),
        'uuid': vm.get('uuid'),
        'description': vm.get('description'),
        'cores': vm.get('num_cores_per_vcpu'),
        'sockets': vm.get('num_vcpus'),
        'memory_gb': vm.get('memory_mb')/1000,
        'power_state': str(vm.get('power_state')).lower(),
        'timezone': vm.get('timezone'),
        'machine_type': vm.get('machine_type'),
        'cdrom': False,
        'disks': [],
        'nics': [],
        'gpus': [],
        'serial_ports': [],
    }

    if vm.get('vm_disk_info'):
        for existing_disk in vm.get('vm_disk_info'):
            container = None
            image = None
            volume_group = None

            if existing_disk.get('storage_container_uuid'):
                container = container_obj.search_uuid(uuid=existing_disk.get('storage_container_uuid'), clusteruuid=clusteruuid)

            if existing_disk.get('source_disk_address'):
                image = image_obj.search_uuid(uuid=existing_disk.get('source_disk_address').get('vmdisk_uuid'), clusteruuid=clusteruuid)

            if existing_disk.get('disk_address').get('volume_group_uuid'):
                volume_group = volume_group_obj.search_volume_groups_uuid(uuid=existing_disk.get('disk_address').get('volume_group_uuid'), clusteruuid=clusteruuid)

            if existing_disk.get('is_cdrom'):
                config['cdrom'] = True

            else:
                disk = {
                    'bus': existing_disk.get('disk_address').get('device_bus'),
                    'index': existing_disk.get('disk_address').get('device_index'),
                    # 'label': existing_disk.get('disk_address').get('disk_label'),
                    'flash_mode': existing_disk.get('flash_mode_enabled'),
                    'size': existing_disk.get('size'),
                }

                if image:
                    disk['image_name'] = image.get('name')
                elif volume_group:
                    disk['volume_group_name'] = volume_group.get('name', None)
                else:
                    disk['storage_container_name'] = container.get('name', None)

                config['disks'].append(disk)

    if vm.get('vm_nics'):
        for existing_nic in vm.get('vm_nics'):
            network = network_obj.search_uuid(uuid=existing_nic.get('network_uuid'), clusteruuid=clusteruuid)
            nic = {
                'network_name': network.get('name') or None,
                'connect': existing_nic.get('is_connected'),
                'mac_address': existing_nic.get('mac_address').lower(),
                'ipam': existing_nic.get('request_ip'),
                'requested_ip_address': existing_nic.get('requested_ip_address'),
            }

            if existing_nic.get('adapter_type'):
                existing_nic['adapter_type'] = existing_nic.get('adapter_type').lower()
            else:
                existing_nic['adapter_type'] = 'e1000'

            config['nics'].append(nic)

    if vm.get('vm_gpus'):
        for existing_gpu in vm.get('vm_gpus'):
            gpu = {
                'device_id': existing_gpu.get('device_id'),
                'gpu_type': existing_gpu.get('gpu_type').lower(),
                'gpu_vendor': existing_gpu.get('gpu_vendor').lower(),
            }
            config['gpus'].append(gpu)

    if vm.get('serial_ports'):
        for existing_serial_port in vm.get('serial_ports'):
            serial_port = {
                'port_index': existing_serial_port.get('index'),
                'port_type': existing_serial_port.get('type').lower(),
            }
            config['serial_ports'].append(serial_port)

    return config


def get_vm(api_client, clusteruuid, name):
    """
    Finds an existing VM generates its specification via `get_existing_vm_spec` and returns the result
    Args:
        api_client: Nutanix API Client object
        clusteruuid: The UUID of the cluster to search within
        name: The name of the VM
    Returns:
        is_error: True or False indicating whether an error occurred during the execution of this function
        is_changed: True or False indicating whether a change was made during the execution of this function
        message: Any messages generated when this function was executed
        config: A dict containing the specification for this VMs configuration
    """
    vms_obj = prism.Vms(api_client=api_client)
    config = {}
    is_error = False
    is_changed = False
    message = ''
    config = {}

    try:
        vm = vms_obj.search_name(name=name, clusteruuid=clusteruuid)
        if vm:
            config = get_existing_vm_spec(api_client=api_client, clusteruuid=clusteruuid, vm=vm)
            message = 'VM found. Data: {0}'.format(vm)

        else:
            message = 'VM not found.'

    except Exception as err:
        is_error = True
        message = 'VM search failed. Data: {0}. Error detail: {1}'.format(vm, err)

    return is_error, is_changed, message, config


def power_state(api_client, clusteruuid, data):
    """
    """
    vms_obj = prism.Vms(api_client=api_client)
    vm = vms_obj.search_name(name=data.get('name'), clusteruuid=clusteruuid)
    is_error = False
    is_changed = False
    message = ''

    power_state_map = {
        'on': 'on',
        'off': 'off',
        'reset': 'reset',
        'guest-reboot': 'acpi_reboot',
        'powercycle': 'powercycle',
        'guest-shutdown': 'acpi_shutdown',
        'resume': 'resume',
        'suspend': 'suspend'
    }

    config = {
        'uuid': vm.get('uuid'),
        'desired_state': power_state_map[data.get('state')],
        'wait': True,
    }

    try:
        is_changed = vms_obj.power_state(clusteruuid=clusteruuid, **config)

        if is_changed:
            message = 'VM power state updated to "{0}"'.format(config['desired_state'])
        else:
            message = 'VM power state was not updated.'

    except Exception as err:
        is_error = True
        message = 'Error changing the VM power state. Error: {0}'.format(err)

    return is_error, is_changed, message


def delete_vm(api_client, clusteruuid, data):
    """
    Deletes the specified virtual machine
    Args:
        api_client: Nutanix API Client object
        clusteruuid: The UUID of the cluster to search within
        data: The ansible module parameters dict
    Returns:
        is_error: True or False indicating whether an error occurred during the execution of this function
        is_changed: True or False indicating whether a change was made during the execution of this function
        message: Any messages generated when this function was executed
    """
    vms_obj = prism.Vms(api_client=api_client)
    is_error = False
    is_changed = False
    message = ''
    config = {
        'name': data.get('name'),
        'snapshots': False,
        'vg_detach': False,
        'wait': True,
    }

    if data.get('force'):
        config['snapshots'] = True
        config['vg_detach'] = True

    try:
        is_changed = vms_obj.delete_name(clusteruuid=clusteruuid, **config)

        if is_changed:
            message = 'VM "{0}" deleted.'.format(config['name'])
        else:
            message = 'VM "{0}" was not deleted.'.format(config['desired_state'])

    except Exception as err:
        is_error = True
        message = 'Unable to delete the VM. Error: {0}'.format(err)

    return is_error, is_changed, message


def validate_vm_spec(api_client, clusteruuid, data):
    """
    Validates the input parameters passed into this module
    Args:
        api_client: Nutanix API Client object
        clusteruuid: The UUID of the cluster to search within
        data: The ansible module parameters dict
    Returns:
        is_error: True or False indicating whether an error occurred during the execution of this function
        is_changed: True or False indicating whether a change was made during the execution of this function
        messages: An array containing any messages generated when this function was executed
        config: A dict containing the specification for this VMs configuration
    """
    is_error = False
    is_changed = False
    messages = []

    config = {
        'name': data.get('name'),
        'cores': data.get('cores'),
        'sockets': data.get('sockets'),
        'memory_gb': data.get('memory_gb'),
        'description': data.get('description'),
        'power_state': 'off',
        'timezone': data.get('timezone'),
        'sysprep': data.get('sysprep'),
        'cloudinit': data.get('cloudinit'),
        'add_cdrom': data.get('add_cdrom'),
        'machine_type': data.get('machine_type'),
        'disks': data.get('disks'),
        'nics': data.get('nics'),
        'gpus': data.get('gpus'),
        'serial_ports': data.get('serial_ports'),
    }

    if config.get('disks'):
        disk_idx = 0
        for disk in config.get('disks'):
            if disk.get('storage_container_name'):
                container = _get_container(api_client=api_client, clusteruuid=clusteruuid, name=disk.get('storage_container_name'))
                if not container:
                    messages.append('The defined container {0} does not exists on the target cluster {1}. Fix disk config and try '
                                    'again.'.format(disk.get('storage_container_name'), clusteruuid))
                    is_error = True

            if disk.get('image_name'):
                image = _get_image(api_client=api_client, clusteruuid=clusteruuid, name=disk.get('image_name'))
                if not image:
                    messages.append('The defined image {0} does not exists on the target cluster {1}. Fix disk config and try '
                                    'again.'.format(disk.get('image_name'), clusteruuid))
                    is_error = True

                # if disk.get('size_gb') < image_size:
                #     messages.append('The size specified when creating a disk from an image must be larger than the image size.')
                #     valid = False

            if disk.get('volume_group_name'):
                volume_group = _get_volume_group(api_client=api_client, clusteruuid=clusteruuid, name=disk.get('volume_group_name'))
                if not volume_group:
                    messages.append('The defined volume group {0} does not exists on the target cluster {1}. Fix disk config and try '
                                    'again.'.format(disk.get('volume_group_name'), clusteruuid))
                    is_error = True

            if disk.get('storage_container_name') and any([disk.get('image_name'), disk.get('volume_group_name')]):
                messages.append('storage_container_name can not be used in conjunction with image_name or volume_group_name. Fix disk config and try again.')
                is_error = True

            elif disk.get('image_name') and any([disk.get('storage_container_name'), disk.get('volume_group_name')]):
                messages.append('image_name can not be used in conjunction with storage_container_name or volume_group_name. Fix disk config and try again.')
                is_error = True

            elif disk.get('volume_group_name') and any([disk.get('storage_container_name'), disk.get('image_name')]):
                messages.append('volume_group_name can not be used in conjunction with storage_container_name or image_name. Fix disk config and try again.')
                is_error = True

            if disk.get('storage_container_name') and not disk.get('size_gb'):
                messages.append('A new disk must have size_gb defined.')
                is_error = True

            if disk.get('size_gb') and disk.get('volume_group_name'):
                messages.append('You cannot specify size_gb and attach a volume group at the same time. To increase the size of the volume group modify the '
                                'volume group directly.')
                is_error = True

            if not disk.get('index'):
                disk['index'] = disk_idx

            if disk.get('size_gb'):
                disk['size'] = disk.get('size_gb') * 1073741824
            disk_idx += 1

    if config.get('nics'):
        for nic in config.get('nics'):
            if not nic.get('network_name'):
                messages.append('You must specify a network_name for this nic to be connected onto.')
                is_error = True

            network = _get_network(api_client=api_client, clusteruuid=clusteruuid, name=nic.get('network_name'))
            if not network:
                messages.append('The defined network {0} does not exists on the target cluster {1}. Fix nic config and try '
                                'again.'.format(nic.get('network_name'), clusteruuid))
                is_error = True

            if not nic.get('ipam') and nic.get('requested_ip_address'):
                messages.append('To allocate an ip address via requested_ip_address the ipam parameter has to be set to True.')
                is_error = True

    return is_error, is_changed, messages, config


def clean_empty(d):
    """
    Removes keys from a dict when their value is set to None, Null or 0
    Args:
        d: A dict to be processed
    Returns: A dict without any None values
    """
    if isinstance(d, dict):
        return {
            k: v
            for k, v in ((k, clean_empty(v)) for k, v in d.items())
            if v
        }
    if isinstance(d, list):
        return [v for v in map(clean_empty, d) if v]
    return d


def create_vm(api_client, clusteruuid, data):
    """
    Creates a new VM after first validating that the module input parameters are correct.
    Args:
        api_client: Nutanix API Client object
        clusteruuid: The UUID of the cluster to search within
        data: The ansible module parameters dict
    Returns:
        is_error: True or False indicating whether an error occurred during the execution of this function
        is_changed: True or False indicating whether a change was made during the execution of this function
        message: An array containing any messages generated when this function was executed
        config: A dict containing the specification for this VMs configuration
    """
    vms_obj = prism.Vms(api_client=api_client)
    is_error, is_changed, message, config = validate_vm_spec(api_client, clusteruuid, data)

    config = clean_empty(config)
    if not is_error:
        try:
            is_changed = vms_obj.create(clusteruuid=clusteruuid, **config)
            if is_changed:
                message = 'VM "{0}" created.'.format(config['name'])
            else:
                message = 'VM "{0}" was not created.'.format(config['desired_state'])

        except Exception as err:
            is_error = True
            message = 'Error when creating the VM. VM Config: {0}. Error Details: {1}'.format(config, err)

    return is_error, is_changed, message


def update_vm(api_client, clusteruuid, existing_vm, target_vm, force):
    """
    Updates a existing VM after first validating that the module input parameters are correct.
    Args:
        api_client: Nutanix API Client object
        clusteruuid: The UUID of the cluster to search within
        existing_vm: A dict describing the existing VM configuration
        target_vm: A dict describing the target VM configuration
        force: Whether to forcibaly shutdown the VM if the change requires. For example, changing the number of vCPUs is a disruptive action which requires the
               VM to be powered off prior to making the change. Without Force set to True this change will fail.
    Returns:
        is_error: True or False indicating whether an error occurred during the execution of this function
        is_changed: True or False indicating whether a change was made during the execution of this function
        message: An array containing any messages generated when this function was executed
    """
    vms_obj = prism.Vms(api_client=api_client)
    is_error = False
    is_changed = False
    change_needed = False
    message = ''

    config = {
        'name': existing_vm.get('name'),
        'force': force,
        'wait': True,
    }

    if target_vm.get('cores') != existing_vm.get('cores'):
        config['cores'] = target_vm.get('cores')
        change_needed = True

    if target_vm.get('sockets') != existing_vm.get('sockets'):
        config['sockets'] = target_vm.get('sockets')
        change_needed = True

    if target_vm.get('memory_gb') != existing_vm.get('memory_gb'):
        config['memory_gb'] = target_vm.get('memory_gb')
        change_needed = True

    if target_vm.get('description') != existing_vm.get('description') and target_vm.get('description') != "":
        config['description'] = target_vm.get('description')
        change_needed = True

    if target_vm.get('timezone') != existing_vm.get('timezone'):
        config['timezone'] = target_vm.get('timezone')
        change_needed = True

    if target_vm.get('add_cdrom') != existing_vm.get('add_cdrom'):
        config['add_cdrom'] = target_vm.get('add_cdrom')
        change_needed = True

    try:
        if change_needed:
            is_changed = vms_obj.update_name(clusteruuid=clusteruuid, **config)
            message = 'Updated the VM. VM Config: {0}.'.format(config)

    except Exception as err:
        is_error = True
        message = 'Error when updating the VM. VM Config: {0}. Error Details: {1}'.format(config, err)

    return is_error, is_changed, message


def main():
    """
    Main function of the module.
    """
    disk_spec = dict(
        bus=dict(default='scsi', choices=['scsi', 'ide', 'pci', 'sata', 'spapr', 'nvme']),
        size_gb=dict(required=False, type='int'),
        storage_container_name=dict(required=False, type='str', default=None),
        image_name=dict(required=False, type='str', default=None),
        volume_group_name=dict(required=False, type='str', default=None),
        flash_mode=dict(required=False, type='bool', default=False),
        # label=dict(required=False, type='str', default=None),
        index=dict(required=False, type='int', default=None)
    )

    nic_spec = dict(
        network_name=dict(required=True, type='str'),
        adaptor_type=dict(default='e1000', choices=['e1000', 'e1000e', 'pcnet32', 'vmxnet', 'vmxnet2', 'vmxnet3']),
        connect=dict(required=False, type='bool', default=True),
        mac_address=dict(required=False, type='str', default=None),
        ipam=dict(required=False, type='bool', default=False),
        requested_ip_address=dict(required=False, type='str', default=None),
    )

    gpu_spec = dict(
        device_id=dict(required=True, type='int'),
        gpu_type=dict(default='pass_through_graphics', choices=['pass_through_graphics']),
        gpu_vendor=dict(default='nvidia', choices=['nvidia']),
    )

    serial_port_spec = dict(
        port_index=dict(required=True, type='int'),
        port_type=dict(default='null', choices=['null', 'server']),
    )
    # power_states = ['on', 'off']
    timezones = ['UTC', ]
    machine_types = ['pc', 'pseries', 'q35']

    argument_spec = prism_argument_spec()
    argument_spec.update(
        state=dict(default='present', choices=['present', 'absent', 'on', 'off', 'guest-reboot', 'guest-shutdown', 'reset', 'powercycle', 'resume', 'suspend']),
        name=dict(required=True, type='str'),
        description=dict(required=False, type='str', default=''),
        cores=dict(required=False, type='int', default=1),
        sockets=dict(required=False, type='int', default=1),
        memory_gb=dict(required=False, type='int', default=1),
        # power_state=dict(default='on', choices=power_states),
        timezone=dict(default='UTC', choices=timezones),
        cdrom=dict(required=False, type='bool', default=True),
        disks=dict(required=False, type='list', elements='dict', options=disk_spec),
        nics=dict(required=False, type='list', elements='dict', options=nic_spec),
        gpus=dict(required=False, type='list', elements='dict', options=gpu_spec),
        serial_ports=dict(required=False, type='list', elements='dict', options=serial_port_spec),
        sysprep=dict(required=False, type='str'),
        cloudinit=dict(required=False, type='str'),
        machine_type=dict(default='pc', choices=machine_types),
        cluster_name=dict(required=False, type='str', default=None),
        force=dict(type='bool', default=False, required=False),
    )

    module = AnsibleModule(
        argument_spec=argument_spec,
        required_if=[
        ],
        mutually_exclusive=[
            ['sysprep', 'cloudinit'],
            # ['disks.storage_container_name', 'disks.image_name', 'disks.volume_group_name'],
        ],
        required_one_of=[
            # ['disks.storage_container_name', 'disks.image_name', 'disks.volume_group_name'],
        ],
        supports_check_mode=True
    )

    if not HAS_NTNX_API:
        module.fail_json(msg=missing_required_lib("ntnx-api"), exception=NTNX_API_IMP_ERR)

    is_error = False
    result = {
        'changed': False,
        'original_message': '',
        'message': '',
        'diff': {
            'before': {},
            'after': {},
        },
        # 'debug': []
    }

    validate_certs_bool = False

    # Prism Connection Options
    prism_ip = module.params.get('prism')
    username = module.params.get('username')
    password = module.params.get('password')
    port = module.params.get('port')
    validate_certs = module.params.get('validate_certs')

    # Module Options
    state = module.params.get('state')
    cluster_name = module.params.get('cluster_name')
    name = module.params.get('name')
    force = module.params.get('force')

    # check API connection
    if validate_certs == 'yes':
        validate_certs_bool = True

    api_client = PrismApi(ip_address=prism_ip, username=username, password=password, port=port, validate_certs=validate_certs_bool)
    if not api_client.test():
        is_error = True
        result['message'] = "Connection to Prism Endpoint failed. {0}".format(api_client)

    # require cluster_name if connecting to a prism central instance
    if api_client.connection_type == 'pc' and not cluster_name:
        is_error = True
        result['message'] = 'When making a change on a multi-cluster environment ie. Prism Central, you need to define the name of the cluster where ' \
                            'the change is to occur.'
    if is_error:
        module.fail_json(msg='If connected to PC please provide cluster_name parameter.', **result)

    # loop through clusters
    # if cluster_name provided only process that specific cluster
    cluster_obj = prism.Cluster(api_client=api_client)
    cluster_uuids = cluster_obj.get_all_uuids()
    for cluster_uuid in cluster_uuids:
        # result['debug'].append('working on cluster "{0}"'.format(cluster_uuids))
        process_cluster = False
        cluster = cluster_obj.get(cluster_uuid)
        if cluster_name:
            if cluster_name.lower() == cluster.get('name').lower():
                process_cluster = True
        else:
            process_cluster = True

        if process_cluster:
            # result['debug'].append('processing cluster "{0}"'.format(cluster_uuids))

            # validate target VM spec
            is_error, result['changed'], result['message'], target_vm_config = validate_vm_spec(api_client=api_client, clusteruuid=cluster_uuid,
                                                                                                data=module.params)
            # result['diff']['_target'] = target_vm_config
            if is_error:
                module.fail_json(msg='Failed to validate provided VM configuration.', **result)

            # check whether VM already exists
            is_error, result['changed'], result['message'], vm_before = get_vm(api_client=api_client, clusteruuid=cluster_uuid, name=name)
            if is_error:
                module.fail_json(msg='Error occured when searching for the vm.', **result)

            if module.check_mode:
                # result['debug'].append('*** check mode ***')
                result['changed'] = False
                result['diff']['after'] = target_vm_config

            if vm_before:
                # result['debug'].append('vm "{0}" exists: data: {1}'.format(name, vm_before))
                result['diff']['before'] = vm_before
                # vm exists
                if state in ['on', 'off', 'guest-reboot', 'guest-shutdown', 'reset', 'powercycle', 'resume', 'suspend']:
                    # change vm power state
                    if vm_before.get('power_state').lower() != state:
                        # result['debug'].append('changing vm power state')
                        if not module.check_mode:
                            is_error, result['changed'], result['message'] = power_state(api_client=api_client, clusteruuid=cluster_uuid, data=module.params)
                            if is_error:
                                module.fail_json(msg='Failed to change VM power state.', **result)

                        else:
                            target_vm_config['power_state'] = state
                            result['message'] = 'Will change VM {0} power state to {1}.'.format(name, state)

                elif state in ['absent', ]:
                    # result['debug'].append('deleting vm')
                    if not module.check_mode:
                        is_error, result['changed'], result['message'] = delete_vm(api_client=api_client, clusteruuid=cluster_uuid, data=module.params)
                        if is_error:
                            module.fail_json(msg='Failed to delete VM.', **result)

                    else:
                        result['diff']['after'] = {}
                        result['message'] = 'Will delete VM {0}.'.format(name)

                elif state in ['present', ]:
                    # check for differences between the desired state and current state & update the vm config
                    is_error, result['changed'], result['message'] = update_vm(api_client=api_client, clusteruuid=cluster_uuid, existing_vm=vm_before,
                                                                               target_vm=target_vm_config, force=force)

            else:
                # vm does not already exist
                # result['debug'].append('vm "{0}" does not exist'.format(name))
                result['diff']['before'] = {}
                if state in ['on', 'off', 'powercycle']:
                    if not module.check_mode:
                        ## create vm
                        is_error, result['changed'], result['message'] = create_vm(api_client=api_client, clusteruuid=cluster_uuid, data=module.params)
                        if is_error:
                            module.fail_json(msg='Failed to create VM.', **result)

                        ## set power state
                        is_error, result['changed'], result['message'] = power_state(api_client=api_client, clusteruuid=cluster_uuid, data=module.params)
                        if is_error:
                            module.fail_json(msg='Failed to set VM power state.', **result)

                    else:
                        target_vm_config['power_state'] = state
                        result['message'] = 'Will create VM {0} then set power state to {1}.'.format(name, state)

                if state in ['guest-reboot', 'guest-shutdown', 'reset', 'resume', 'suspend']:
                    module.fail_json(msg='Cannot change VM {0} power state to {1} as it does not exist.'.format(name, state), **result)

                elif state in ['absent', ]:
                    result['message'] = 'nothing to do. VM {0} already does not exist.'.format(name, state)

                elif state in ['present', ]:
                    # create vm
                    # result['debug'].append('Creating VM {0}.'.format(name))
                    if not module.check_mode:
                        is_error, result['changed'], result['message'] = create_vm(api_client=api_client, clusteruuid=cluster_uuid, data=module.params)
                        if is_error:
                            module.fail_json(msg='Failed to create VM.', **result)
                    else:
                        result['message'] = 'Will create VM {0}.'.format(name, state)

            if result['changed']:
                # result['debug'].append('vm changed.')
                # result['debug'].append('updating vm_after with new vm configuration.')
                is_error, _, _, result['diff']['after'] = get_vm(api_client=api_client, clusteruuid=cluster_uuid, name=name)

            if module.check_mode:
                # result['debug'].append('*** check mode ***')
                result['changed'] = False
                result['diff']['after'] = target_vm_config

    if is_error:
        module.fail_json(msg=result['message'], **result)

    else:
        module.exit_json(**result)


if __name__ == '__main__':
    main()
