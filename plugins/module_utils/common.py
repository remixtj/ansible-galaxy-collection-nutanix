# -*- coding: utf-8 -*-
# Copyright: (c) 2015, Joseph Callen <jcallen () csc.com>
# Copyright: (c) 2018, Ansible Project
# Copyright: (c) 2018, James E. King III (@jeking3) <jking@apache.org>
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)

from __future__ import absolute_import, division, print_function

__metaclass__ = type


def prism_argument_spec():
    return dict(
        prism=dict(
            type='str',
            required=False,
        ),
        username=dict(
            type='str',
            required=False,
            default='admin',
        ),
        password=dict(
            type='str',
            required=False,
            no_log=True,
        ),
        port=dict(
            type='int',
            default=9440,
        ),
        validate_certs=dict(
            type='str',
            required=False,
            default='yes',
            choices=['yes', 'no', ]
        ),
    )