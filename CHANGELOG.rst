==============================
grdavies.nutanix Release Notes
==============================

.. contents:: Topics


v0.0.9
======

Minor Changes
-------------

- prism_vm - Added VM UUID to returned values to enable the ability to leverage API calls to the Nutanix API via native Ansible uri module if desired.

New Modules
-----------

- grdavies.nutanix.prism_vm_info - Gather virtual machine information from either Nutanix Prism Element or Nutanix Prism Central.

v0.0.8
======

Bugfixes
--------

- prism_vm - Removed mandatory flag on parameters cores & memory_gb to allow for power control when specifing just the VM name.
- prism_vm - Updated power state logic

v0.0.7
======

Minor Changes
-------------

- prism_vm - Commented out disk variable list from mutually_exclusive and required_one_of. Will have to revisit this in a later release.

v0.0.6
======

Minor Changes
-------------

- README.md - Updated ntnx-api package version in README.md Required Python libraries section.

v0.0.5
======

Minor Changes
-------------

- prism_vm - Removed debugging messages from the returned results.

v0.0.4
======

New Modules
-----------

- grdavies.nutanix.prism_vm - Manages virtual machines in either Nutanix Prism Element or Nutanix Prism Central.

v0.0.2
======

Bugfixes
--------

- Updated URLs within galaxy.xml to properly reflect updated github repository.

v0.0.1
======

Release Summary
---------------

This is the first proper release of the ``grdavies.nutanix`` collection.
The changelog describes all changes made to the modules and plugins included in this collection.


New Plugins
-----------

Inventory
~~~~~~~~~

- grdavies.nutanix.prism_inventory - Inventory plugin to dynamically generate an inventory from provided Prism Element/Central endpoints.
