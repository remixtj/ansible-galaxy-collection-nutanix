.. _grdavies.nutanix.prism_vm_info_module:


******************************
grdavies.nutanix.prism_vm_info
******************************

**Gather virtual machine information from either Nutanix Prism Element or Nutanix Prism Central.**


Version added: 0.0.9

.. contents::
   :local:
   :depth: 1


Synopsis
--------
- This module will return the configuration of an existing virtual machine.



Requirements
------------
The below requirements are needed on the host that executes this module.

- python >= 2.7
- ntnx-api >= 1.1.30


Parameters
----------

.. raw:: html

    <table  border=0 cellpadding=0 class="documentation-table">
        <tr>
            <th colspan="1">Parameter</th>
            <th>Choices/<font color="blue">Defaults</font></th>
            <th width="100%">Comments</th>
        </tr>
            <tr>
                <td colspan="1">
                    <div class="ansibleOptionAnchor" id="parameter-"></div>
                    <b>cluster_name</b>
                    <a class="ansibleOptionLink" href="#parameter-" title="Permalink to this option"></a>
                    <div style="font-size: small">
                        <span style="color: purple">string</span>
                    </div>
                </td>
                <td>
                </td>
                <td>
                        <div>If connecting to an instance of Prism Central this field is mandatory.</div>
                        <div>Set to the name of a Prism Element cluster where the virtual machine exists.</div>
                </td>
            </tr>
            <tr>
                <td colspan="1">
                    <div class="ansibleOptionAnchor" id="parameter-"></div>
                    <b>name</b>
                    <a class="ansibleOptionLink" href="#parameter-" title="Permalink to this option"></a>
                    <div style="font-size: small">
                        <span style="color: purple">string</span>
                    </div>
                </td>
                <td>
                </td>
                <td>
                        <div>The name of the VM to get info on.</div>
                        <div>This is required if <code>uuid</code> is not supplied.</div>
                </td>
            </tr>
            <tr>
                <td colspan="1">
                    <div class="ansibleOptionAnchor" id="parameter-"></div>
                    <b>password</b>
                    <a class="ansibleOptionLink" href="#parameter-" title="Permalink to this option"></a>
                    <div style="font-size: small">
                        <span style="color: purple">string</span>
                         / <span style="color: red">required</span>
                    </div>
                </td>
                <td>
                </td>
                <td>
                        <div>The password for either Nutanix Prism Element or Nutanix Prism Central for authentication.</div>
                </td>
            </tr>
            <tr>
                <td colspan="1">
                    <div class="ansibleOptionAnchor" id="parameter-"></div>
                    <b>port</b>
                    <a class="ansibleOptionLink" href="#parameter-" title="Permalink to this option"></a>
                    <div style="font-size: small">
                        <span style="color: purple">integer</span>
                    </div>
                </td>
                <td>
                </td>
                <td>
                        <div>The TCP port for either Nutanix Prism Element or Nutanix Prism Central to connect.</div>
                </td>
            </tr>
            <tr>
                <td colspan="1">
                    <div class="ansibleOptionAnchor" id="parameter-"></div>
                    <b>prism</b>
                    <a class="ansibleOptionLink" href="#parameter-" title="Permalink to this option"></a>
                    <div style="font-size: small">
                        <span style="color: purple">string</span>
                         / <span style="color: red">required</span>
                    </div>
                </td>
                <td>
                </td>
                <td>
                        <div>The IP address or hostname for either Nutanix Prism Element or Nutanix Prism Central to connect.</div>
                </td>
            </tr>
            <tr>
                <td colspan="1">
                    <div class="ansibleOptionAnchor" id="parameter-"></div>
                    <b>username</b>
                    <a class="ansibleOptionLink" href="#parameter-" title="Permalink to this option"></a>
                    <div style="font-size: small">
                        <span style="color: purple">string</span>
                    </div>
                </td>
                <td>
                </td>
                <td>
                        <div>The username for either Nutanix Prism Element or Nutanix Prism Central for authentication.</div>
                </td>
            </tr>
            <tr>
                <td colspan="1">
                    <div class="ansibleOptionAnchor" id="parameter-"></div>
                    <b>uuid</b>
                    <a class="ansibleOptionLink" href="#parameter-" title="Permalink to this option"></a>
                    <div style="font-size: small">
                        <span style="color: purple">-</span>
                    </div>
                </td>
                <td>
                </td>
                <td>
                        <div>The UUID of the VM to get info on.</div>
                        <div>This is required if <code>name</code> is not supplied.</div>
                </td>
            </tr>
            <tr>
                <td colspan="1">
                    <div class="ansibleOptionAnchor" id="parameter-"></div>
                    <b>validate_certs</b>
                    <a class="ansibleOptionLink" href="#parameter-" title="Permalink to this option"></a>
                    <div style="font-size: small">
                        <span style="color: purple">string</span>
                    </div>
                </td>
                <td>
                        <ul style="margin: 0; padding: 0"><b>Choices:</b>
                                    <li><div style="color: blue"><b>yes</b>&nbsp;&larr;</div></li>
                                    <li>no</li>
                        </ul>
                </td>
                <td>
                </td>
            </tr>
    </table>
    <br/>


Notes
-----

.. note::
   - Ensure that the user account used to connect to Prism Element or Prism Central has the appropriate rights to query the VM.
   - Although VM names do not have on each Nutanix cluster, this module assumes that within each individual cluster there is only one instance of each virtual machine name.






Return Values
-------------
Common return values are documented `here <https://docs.ansible.com/ansible/latest/reference_appendices/common_return_values.html#common-return-values>`_, the following are the fields unique to this module:

.. raw:: html

    <table border=0 cellpadding=0 class="documentation-table">
        <tr>
            <th colspan="1">Key</th>
            <th>Returned</th>
            <th width="100%">Description</th>
        </tr>
            <tr>
                <td colspan="1">
                    <div class="ansibleOptionAnchor" id="return-"></div>
                    <b>vm_info</b>
                    <a class="ansibleOptionLink" href="#return-" title="Permalink to this return value"></a>
                    <div style="font-size: small">
                      <span style="color: purple">dictionary</span>
                    </div>
                </td>
                <td>always</td>
                <td>
                            <div>metadata about the VM.</div>
                    <br/>
                        <div style="font-size: smaller"><b>Sample:</b></div>
                        <div style="font-size: smaller; color: blue; word-wrap: break-word; word-break: break-all;">{&#x27;allow_live_migrate&#x27;: True, &#x27;gpus_assigned&#x27;: False, &#x27;ha_priority&#x27;: 0, &#x27;host_uuid&#x27;: &#x27;c60c1dea-f858-4ee5-ba76-6701819f2837&#x27;, &#x27;memory_mb&#x27;: 4096, &#x27;name&#x27;: &#x27;gitlb_runner&#x27;, &#x27;num_cores_per_vcpu&#x27;: 1, &#x27;num_vcpus&#x27;: 2, &#x27;power_state&#x27;: &#x27;on&#x27;, &#x27;timezone&#x27;: &#x27;America/Phoenix&#x27;, &#x27;uuid&#x27;: &#x27;2afd6b6a-e55a-489d-99d3-0ed553e6616d&#x27;, &#x27;vm_disk_info&#x27;: [{&#x27;disk_address&#x27;: {&#x27;device_bus&#x27;: &#x27;ide&#x27;, &#x27;device_index&#x27;: 0, &#x27;disk_label&#x27;: &#x27;ide.0&#x27;}, &#x27;is_cdrom&#x27;: True, &#x27;is_empty&#x27;: True, &#x27;flash_mode_enabled&#x27;: False, &#x27;is_scsi_passthrough&#x27;: True, &#x27;is_hot_remove_enabled&#x27;: False, &#x27;is_thin_provisioned&#x27;: False, &#x27;shared&#x27;: False}, {&#x27;disk_address&#x27;: {&#x27;device_bus&#x27;: &#x27;scsi&#x27;, &#x27;device_index&#x27;: 0, &#x27;disk_label&#x27;: &#x27;scsi.0&#x27;, &#x27;vmdisk_uuid&#x27;: &#x27;d19747af-1f1e-4abd-8f24-89c7e52ba375&#x27;}, &#x27;is_cdrom&#x27;: False, &#x27;is_empty&#x27;: False, &#x27;flash_mode_enabled&#x27;: False, &#x27;is_scsi_passthrough&#x27;: True, &#x27;is_hot_remove_enabled&#x27;: True, &#x27;is_thin_provisioned&#x27;: False, &#x27;shared&#x27;: False, &#x27;source_disk_address&#x27;: {&#x27;vmdisk_uuid&#x27;: &#x27;1841fdc7-a095-48e0-ba46-13641c341cf8&#x27;}, &#x27;storage_container_uuid&#x27;: &#x27;4c1979c1-8b56-4db8-aa3c-8e2906344d5d&#x27;, &#x27;size&#x27;: 214748364800}], &#x27;vm_features&#x27;: {&#x27;AGENT_VM&#x27;: False, &#x27;VGA_CONSOLE&#x27;: True}, &#x27;vm_logical_timestamp&#x27;: 15, &#x27;vm_nics&#x27;: [{&#x27;mac_address&#x27;: &#x27;50:6b:8d:c0:c0:53&#x27;, &#x27;network_uuid&#x27;: &#x27;fe48a7d5-5fce-4c8b-b895-68c11e93b81c&#x27;, &#x27;model&#x27;: &#x27;&#x27;, &#x27;ip_address&#x27;: &#x27;192.168.1.119&#x27;, &#x27;is_connected&#x27;: True}]}</div>
                </td>
            </tr>
    </table>
    <br/><br/>


Status
------


Authors
~~~~~~~

- Ross Davies <davies.ross@gmail.com>
