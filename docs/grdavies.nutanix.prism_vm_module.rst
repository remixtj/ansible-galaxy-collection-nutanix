.. _grdavies.nutanix.prism_vm_module:


*************************
grdavies.nutanix.prism_vm
*************************

**Manages virtual machines in either Nutanix Prism Element or Nutanix Prism Central.**


Version added: 0.0.4

.. contents::
   :local:
   :depth: 1


Synopsis
--------
- This module can be used to create new virtual machines, manage the power state of virtual machine such as power on, power off, restart etc. Modify various virtual machine components like cpu, memory, network, disk etc. Finally it is also possible to remove a virtual machine along with its associated configuration.



Requirements
------------
The below requirements are needed on the host that executes this module.

- python >= 2.7
- ntnx-api >= 1.1.30


Parameters
----------

.. raw:: html

    <table  border=0 cellpadding=0 class="documentation-table">
        <tr>
            <th colspan="2">Parameter</th>
            <th>Choices/<font color="blue">Defaults</font></th>
            <th width="100%">Comments</th>
        </tr>
            <tr>
                <td colspan="2">
                    <div class="ansibleOptionAnchor" id="parameter-"></div>
                    <b>cdrom</b>
                    <a class="ansibleOptionLink" href="#parameter-" title="Permalink to this option"></a>
                    <div style="font-size: small">
                        <span style="color: purple">boolean</span>
                    </div>
                </td>
                <td>
                        <ul style="margin: 0; padding: 0"><b>Choices:</b>
                                    <li>no</li>
                                    <li><div style="color: blue"><b>yes</b>&nbsp;&larr;</div></li>
                        </ul>
                </td>
                <td>
                        <div>Whether to add a cdrom drive to the virtual machine.</div>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <div class="ansibleOptionAnchor" id="parameter-"></div>
                    <b>cloudinit</b>
                    <a class="ansibleOptionLink" href="#parameter-" title="Permalink to this option"></a>
                    <div style="font-size: small">
                        <span style="color: purple">-</span>
                    </div>
                </td>
                <td>
                </td>
                <td>
                        <div>The cloudinit content in text format.</div>
                        <div>Should only be provided for linux systems being deployed.</div>
                        <div>Only applies to VM creation tasks.</div>
                        <div>Is mutually exclusive with <code>sysprep</code></div>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <div class="ansibleOptionAnchor" id="parameter-"></div>
                    <b>cluster_name</b>
                    <a class="ansibleOptionLink" href="#parameter-" title="Permalink to this option"></a>
                    <div style="font-size: small">
                        <span style="color: purple">string</span>
                    </div>
                </td>
                <td>
                </td>
                <td>
                        <div>If connecting to an instance of Prism Central this field is mandatory.</div>
                        <div>Set to the name of a Prism Element cluster where the virtual machine is to be created, modified or have its power state changed.</div>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <div class="ansibleOptionAnchor" id="parameter-"></div>
                    <b>cores</b>
                    <a class="ansibleOptionLink" href="#parameter-" title="Permalink to this option"></a>
                    <div style="font-size: small">
                        <span style="color: purple">integer</span>
                    </div>
                </td>
                <td>
                </td>
                <td>
                        <div>The number of virtual cores / virtual CPUs to set on the virtual machine.</div>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <div class="ansibleOptionAnchor" id="parameter-"></div>
                    <b>disks</b>
                    <a class="ansibleOptionLink" href="#parameter-" title="Permalink to this option"></a>
                    <div style="font-size: small">
                        <span style="color: purple">list</span>
                         / <span style="color: purple">elements=dictionary</span>
                    </div>
                </td>
                <td>
                </td>
                <td>
                        <div>A list of disks to add in order.</div>
                        <div>Reducing disk size below what is already provisioned is not supported.</div>
                        <div>Removing the virtual machines existing disks is not supported.</div>
                        <div>Only one of <code>disks.storage_container_name</code> <code>disks.image_name</code> or <code>disks.volume_group_name</code> can be provided per-disk.</div>
                        <div>Currently only applies to VM creation tasks.</div>
                </td>
            </tr>
                                <tr>
                    <td class="elbow-placeholder"></td>
                <td colspan="1">
                    <div class="ansibleOptionAnchor" id="parameter-"></div>
                    <b>bus</b>
                    <a class="ansibleOptionLink" href="#parameter-" title="Permalink to this option"></a>
                    <div style="font-size: small">
                        <span style="color: purple">string</span>
                    </div>
                </td>
                <td>
                        <ul style="margin: 0; padding: 0"><b>Choices:</b>
                                    <li><div style="color: blue"><b>scsi</b>&nbsp;&larr;</div></li>
                                    <li>ide</li>
                                    <li>pci</li>
                                    <li>sata</li>
                                    <li>spapr</li>
                                    <li>nvme</li>
                        </ul>
                </td>
                <td>
                        <div>The type of bus to attach the virtual machine disk onto.</div>
                </td>
            </tr>
            <tr>
                    <td class="elbow-placeholder"></td>
                <td colspan="1">
                    <div class="ansibleOptionAnchor" id="parameter-"></div>
                    <b>flash_mode</b>
                    <a class="ansibleOptionLink" href="#parameter-" title="Permalink to this option"></a>
                    <div style="font-size: small">
                        <span style="color: purple">boolean</span>
                    </div>
                </td>
                <td>
                        <ul style="margin: 0; padding: 0"><b>Choices:</b>
                                    <li><div style="color: blue"><b>no</b>&nbsp;&larr;</div></li>
                                    <li>yes</li>
                        </ul>
                </td>
                <td>
                        <div>Whether to enable flash mode on this virtual disk</div>
                </td>
            </tr>
            <tr>
                    <td class="elbow-placeholder"></td>
                <td colspan="1">
                    <div class="ansibleOptionAnchor" id="parameter-"></div>
                    <b>image_name</b>
                    <a class="ansibleOptionLink" href="#parameter-" title="Permalink to this option"></a>
                    <div style="font-size: small">
                        <span style="color: purple">string</span>
                    </div>
                </td>
                <td>
                </td>
                <td>
                        <div>The name for the image in the image service to clone for this virtual disk.</div>
                        <div>The image has to be present.</div>
                        <div>Mutually exclusive with <code>storage_container_name</code> and <code>volume_group_name</code></div>
                </td>
            </tr>
            <tr>
                    <td class="elbow-placeholder"></td>
                <td colspan="1">
                    <div class="ansibleOptionAnchor" id="parameter-"></div>
                    <b>size_gb</b>
                    <a class="ansibleOptionLink" href="#parameter-" title="Permalink to this option"></a>
                    <div style="font-size: small">
                        <span style="color: purple">-</span>
                         / <span style="color: red">required</span>
                    </div>
                </td>
                <td>
                </td>
                <td>
                        <div>Disk size in GB.</div>
                        <div>Only applicable when creating a new virtual disk or cloning from an image.</div>
                        <div>Has to be larger than the size of the image being used.</div>
                </td>
            </tr>
            <tr>
                    <td class="elbow-placeholder"></td>
                <td colspan="1">
                    <div class="ansibleOptionAnchor" id="parameter-"></div>
                    <b>storage_container_name</b>
                    <a class="ansibleOptionLink" href="#parameter-" title="Permalink to this option"></a>
                    <div style="font-size: small">
                        <span style="color: purple">string</span>
                    </div>
                </td>
                <td>
                </td>
                <td>
                        <div>The name for the container on which to create this virtual disk.</div>
                        <div>Mutually exclusive with <code>image_name</code> and <code>volume_group_name</code></div>
                </td>
            </tr>
            <tr>
                    <td class="elbow-placeholder"></td>
                <td colspan="1">
                    <div class="ansibleOptionAnchor" id="parameter-"></div>
                    <b>volume_group_name</b>
                    <a class="ansibleOptionLink" href="#parameter-" title="Permalink to this option"></a>
                    <div style="font-size: small">
                        <span style="color: purple">string</span>
                    </div>
                </td>
                <td>
                </td>
                <td>
                        <div>The name for the volume group to attach to this virtual machine.</div>
                        <div>The volume group has to be present.</div>
                        <div>Be wary of the default OS operation when detecting an iSCSI attached volume. Attaching a shared drive already in use elsewhere can cause data availability issues.</div>
                        <div>Mutually exclusive with <code>storage_container_name</code> and <code>image_name</code></div>
                </td>
            </tr>

            <tr>
                <td colspan="2">
                    <div class="ansibleOptionAnchor" id="parameter-"></div>
                    <b>force</b>
                    <a class="ansibleOptionLink" href="#parameter-" title="Permalink to this option"></a>
                    <div style="font-size: small">
                        <span style="color: purple">boolean</span>
                    </div>
                </td>
                <td>
                        <ul style="margin: 0; padding: 0"><b>Choices:</b>
                                    <li>no</li>
                                    <li><div style="color: blue"><b>yes</b>&nbsp;&larr;</div></li>
                        </ul>
                </td>
                <td>
                        <div>If the VM exists and its power state does not allow the requested change to be made and this option is set to False then the task will fail.</div>
                        <div>If the VM exists and its power state does not allow the requested change to be made and this option is set to True then the virtual machines power state will be changed to all the request change to be implemented before being returned to its original value.</div>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <div class="ansibleOptionAnchor" id="parameter-"></div>
                    <b>gpus</b>
                    <a class="ansibleOptionLink" href="#parameter-" title="Permalink to this option"></a>
                    <div style="font-size: small">
                        <span style="color: purple">list</span>
                         / <span style="color: purple">elements=dictionary</span>
                    </div>
                </td>
                <td>
                </td>
                <td>
                        <div>A list of GPUs to add in order.</div>
                        <div>Currently only applies to VM creation tasks.</div>
                </td>
            </tr>
                                <tr>
                    <td class="elbow-placeholder"></td>
                <td colspan="1">
                    <div class="ansibleOptionAnchor" id="parameter-"></div>
                    <b>device_id</b>
                    <a class="ansibleOptionLink" href="#parameter-" title="Permalink to this option"></a>
                    <div style="font-size: small">
                        <span style="color: purple">integer</span>
                         / <span style="color: red">required</span>
                    </div>
                </td>
                <td>
                </td>
                <td>
                        <div>TBD.</div>
                </td>
            </tr>
            <tr>
                    <td class="elbow-placeholder"></td>
                <td colspan="1">
                    <div class="ansibleOptionAnchor" id="parameter-"></div>
                    <b>gpu_type</b>
                    <a class="ansibleOptionLink" href="#parameter-" title="Permalink to this option"></a>
                    <div style="font-size: small">
                        <span style="color: purple">-</span>
                    </div>
                </td>
                <td>
                        <ul style="margin: 0; padding: 0"><b>Choices:</b>
                                    <li><div style="color: blue"><b>pass_through_graphics</b>&nbsp;&larr;</div></li>
                        </ul>
                </td>
                <td>
                        <div>The type of GPU to add to the virtual machine.</div>
                </td>
            </tr>
            <tr>
                    <td class="elbow-placeholder"></td>
                <td colspan="1">
                    <div class="ansibleOptionAnchor" id="parameter-"></div>
                    <b>gpu_vendor</b>
                    <a class="ansibleOptionLink" href="#parameter-" title="Permalink to this option"></a>
                    <div style="font-size: small">
                        <span style="color: purple">-</span>
                    </div>
                </td>
                <td>
                        <ul style="margin: 0; padding: 0"><b>Choices:</b>
                                    <li><div style="color: blue"><b>nvidia</b>&nbsp;&larr;</div></li>
                        </ul>
                </td>
                <td>
                        <div>The GPU vendor.</div>
                </td>
            </tr>

            <tr>
                <td colspan="2">
                    <div class="ansibleOptionAnchor" id="parameter-"></div>
                    <b>machine_type</b>
                    <a class="ansibleOptionLink" href="#parameter-" title="Permalink to this option"></a>
                    <div style="font-size: small">
                        <span style="color: purple">-</span>
                    </div>
                </td>
                <td>
                        <ul style="margin: 0; padding: 0"><b>Choices:</b>
                                    <li><div style="color: blue"><b>pc</b>&nbsp;&larr;</div></li>
                                    <li>pseries</li>
                                    <li>q35</li>
                        </ul>
                </td>
                <td>
                        <div>The architecture for the cluster hosting the virtual machine.</div>
                        <div><code>pc</code> is to be used for x86 clusters.</div>
                        <div><code>pseries</code> is to be used for IBM P-Series clusters.</div>
                        <div><code>q35</code> is to be used for systems running the Intel Q35 embedded systems chipset.</div>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <div class="ansibleOptionAnchor" id="parameter-"></div>
                    <b>memory_gb</b>
                    <a class="ansibleOptionLink" href="#parameter-" title="Permalink to this option"></a>
                    <div style="font-size: small">
                        <span style="color: purple">integer</span>
                         / <span style="color: red">required</span>
                    </div>
                </td>
                <td>
                </td>
                <td>
                        <div>The amount of virtual memory in GB to set on the virtual machine.</div>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <div class="ansibleOptionAnchor" id="parameter-"></div>
                    <b>name</b>
                    <a class="ansibleOptionLink" href="#parameter-" title="Permalink to this option"></a>
                    <div style="font-size: small">
                        <span style="color: purple">string</span>
                         / <span style="color: red">required</span>
                    </div>
                </td>
                <td>
                </td>
                <td>
                        <div>The name of the virtual machine to work with.</div>
                        <div>Virtual machine names within Prism are not necessarily unique although it would be a best practice to ensure uniqueness within each Nutanix cluster. For the purposes of this module it is assumed that the virtual machine names have been kept unique and that if duplicate occur they are in different clusters (ie. VMs with the same name under management by Prism Central are ok as long as they are located in seperate clusters).</div>
                        <div>This parameter is required.</div>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <div class="ansibleOptionAnchor" id="parameter-"></div>
                    <b>nics</b>
                    <a class="ansibleOptionLink" href="#parameter-" title="Permalink to this option"></a>
                    <div style="font-size: small">
                        <span style="color: purple">list</span>
                         / <span style="color: purple">elements=dictionary</span>
                    </div>
                </td>
                <td>
                </td>
                <td>
                        <div>A list of network interfaces to add in order.</div>
                        <div>Currently only applies to VM creation tasks.</div>
                </td>
            </tr>
                                <tr>
                    <td class="elbow-placeholder"></td>
                <td colspan="1">
                    <div class="ansibleOptionAnchor" id="parameter-"></div>
                    <b>adaptor_type</b>
                    <a class="ansibleOptionLink" href="#parameter-" title="Permalink to this option"></a>
                    <div style="font-size: small">
                        <span style="color: purple">string</span>
                    </div>
                </td>
                <td>
                        <ul style="margin: 0; padding: 0"><b>Choices:</b>
                                    <li><div style="color: blue"><b>e1000</b>&nbsp;&larr;</div></li>
                                    <li>e1000e</li>
                                    <li>pcnet32</li>
                                    <li>vmxnet</li>
                                    <li>vmxnet2</li>
                                    <li>vmxnet3</li>
                        </ul>
                </td>
                <td>
                        <div>The type of the network adaptor being added.</div>
                        <div>Not required for AHV.</div>
                </td>
            </tr>
            <tr>
                    <td class="elbow-placeholder"></td>
                <td colspan="1">
                    <div class="ansibleOptionAnchor" id="parameter-"></div>
                    <b>connect</b>
                    <a class="ansibleOptionLink" href="#parameter-" title="Permalink to this option"></a>
                    <div style="font-size: small">
                        <span style="color: purple">boolean</span>
                    </div>
                </td>
                <td>
                        <ul style="margin: 0; padding: 0"><b>Choices:</b>
                                    <li>no</li>
                                    <li><div style="color: blue"><b>yes</b>&nbsp;&larr;</div></li>
                        </ul>
                </td>
                <td>
                        <div>Whether the network interface should be in a connected state or not.</div>
                        <div>Setting this to <code>True</code> will put the network interface into a connected state.</div>
                        <div>Setting this to <code>False</code> will put the network interface into a disconnected state.</div>
                </td>
            </tr>
            <tr>
                    <td class="elbow-placeholder"></td>
                <td colspan="1">
                    <div class="ansibleOptionAnchor" id="parameter-"></div>
                    <b>ipam</b>
                    <a class="ansibleOptionLink" href="#parameter-" title="Permalink to this option"></a>
                    <div style="font-size: small">
                        <span style="color: purple">boolean</span>
                    </div>
                </td>
                <td>
                        <ul style="margin: 0; padding: 0"><b>Choices:</b>
                                    <li><div style="color: blue"><b>no</b>&nbsp;&larr;</div></li>
                                    <li>yes</li>
                        </ul>
                </td>
                <td>
                        <div>Whether to use AHV IPAM to assign an ip address on this network connection.</div>
                </td>
            </tr>
            <tr>
                    <td class="elbow-placeholder"></td>
                <td colspan="1">
                    <div class="ansibleOptionAnchor" id="parameter-"></div>
                    <b>mac_address</b>
                    <a class="ansibleOptionLink" href="#parameter-" title="Permalink to this option"></a>
                    <div style="font-size: small">
                        <span style="color: purple">string</span>
                    </div>
                </td>
                <td>
                </td>
                <td>
                        <div>A custom MAC address for this network adaptor.</div>
                </td>
            </tr>
            <tr>
                    <td class="elbow-placeholder"></td>
                <td colspan="1">
                    <div class="ansibleOptionAnchor" id="parameter-"></div>
                    <b>network_name</b>
                    <a class="ansibleOptionLink" href="#parameter-" title="Permalink to this option"></a>
                    <div style="font-size: small">
                        <span style="color: purple">string</span>
                         / <span style="color: red">required</span>
                    </div>
                </td>
                <td>
                </td>
                <td>
                        <div>The name of the AHV network to connect this network interface onto.</div>
                </td>
            </tr>
            <tr>
                    <td class="elbow-placeholder"></td>
                <td colspan="1">
                    <div class="ansibleOptionAnchor" id="parameter-"></div>
                    <b>requested_ip_address</b>
                    <a class="ansibleOptionLink" href="#parameter-" title="Permalink to this option"></a>
                    <div style="font-size: small">
                        <span style="color: purple">string</span>
                    </div>
                </td>
                <td>
                </td>
                <td>
                        <div>Defines the IP address to assign to this network adaptor rather than assigning a random IP address from the networks IP pool.</div>
                        <div>Requires <code>nic.ipam</code> to be set to True.</div>
                </td>
            </tr>

            <tr>
                <td colspan="2">
                    <div class="ansibleOptionAnchor" id="parameter-"></div>
                    <b>password</b>
                    <a class="ansibleOptionLink" href="#parameter-" title="Permalink to this option"></a>
                    <div style="font-size: small">
                        <span style="color: purple">string</span>
                         / <span style="color: red">required</span>
                    </div>
                </td>
                <td>
                </td>
                <td>
                        <div>The password for either Nutanix Prism Element or Nutanix Prism Central for authentication.</div>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <div class="ansibleOptionAnchor" id="parameter-"></div>
                    <b>port</b>
                    <a class="ansibleOptionLink" href="#parameter-" title="Permalink to this option"></a>
                    <div style="font-size: small">
                        <span style="color: purple">integer</span>
                    </div>
                </td>
                <td>
                </td>
                <td>
                        <div>The TCP port for either Nutanix Prism Element or Nutanix Prism Central to connect.</div>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <div class="ansibleOptionAnchor" id="parameter-"></div>
                    <b>prism</b>
                    <a class="ansibleOptionLink" href="#parameter-" title="Permalink to this option"></a>
                    <div style="font-size: small">
                        <span style="color: purple">string</span>
                         / <span style="color: red">required</span>
                    </div>
                </td>
                <td>
                </td>
                <td>
                        <div>The IP address or hostname for either Nutanix Prism Element or Nutanix Prism Central to connect.</div>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <div class="ansibleOptionAnchor" id="parameter-"></div>
                    <b>serial_ports</b>
                    <a class="ansibleOptionLink" href="#parameter-" title="Permalink to this option"></a>
                    <div style="font-size: small">
                        <span style="color: purple">list</span>
                         / <span style="color: purple">elements=dictionary</span>
                    </div>
                </td>
                <td>
                </td>
                <td>
                        <div>A list of serial port interfaces to add in order.</div>
                        <div>Currently only applies to VM creation tasks.</div>
                </td>
            </tr>
                                <tr>
                    <td class="elbow-placeholder"></td>
                <td colspan="1">
                    <div class="ansibleOptionAnchor" id="parameter-"></div>
                    <b>port_index</b>
                    <a class="ansibleOptionLink" href="#parameter-" title="Permalink to this option"></a>
                    <div style="font-size: small">
                        <span style="color: purple">integer</span>
                         / <span style="color: red">required</span>
                    </div>
                </td>
                <td>
                </td>
                <td>
                        <div>TBD</div>
                </td>
            </tr>
            <tr>
                    <td class="elbow-placeholder"></td>
                <td colspan="1">
                    <div class="ansibleOptionAnchor" id="parameter-"></div>
                    <b>port_type</b>
                    <a class="ansibleOptionLink" href="#parameter-" title="Permalink to this option"></a>
                    <div style="font-size: small">
                        <span style="color: purple">-</span>
                    </div>
                </td>
                <td>
                        <ul style="margin: 0; padding: 0"><b>Choices:</b>
                                    <li><div style="color: blue"><b>null</b>&nbsp;&larr;</div></li>
                                    <li>server</li>
                        </ul>
                </td>
                <td>
                        <div>TBD</div>
                </td>
            </tr>

            <tr>
                <td colspan="2">
                    <div class="ansibleOptionAnchor" id="parameter-"></div>
                    <b>sockets</b>
                    <a class="ansibleOptionLink" href="#parameter-" title="Permalink to this option"></a>
                    <div style="font-size: small">
                        <span style="color: purple">integer</span>
                         / <span style="color: red">required</span>
                    </div>
                </td>
                <td>
                </td>
                <td>
                        <div>The number of virtual sockets to set on the virtual machine.</div>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <div class="ansibleOptionAnchor" id="parameter-"></div>
                    <b>state</b>
                    <a class="ansibleOptionLink" href="#parameter-" title="Permalink to this option"></a>
                    <div style="font-size: small">
                        <span style="color: purple">string</span>
                         / <span style="color: red">required</span>
                    </div>
                </td>
                <td>
                        <ul style="margin: 0; padding: 0"><b>Choices:</b>
                                    <li>absent</li>
                                    <li><div style="color: blue"><b>present</b>&nbsp;&larr;</div></li>
                                    <li>on</li>
                                    <li>off</li>
                                    <li>guest-reboot</li>
                                    <li>guest-shutdown</li>
                                    <li>reset</li>
                                    <li>powercycle</li>
                                    <li>resume</li>
                                    <li>suspend</li>
                        </ul>
                </td>
                <td>
                        <div>Specify the state that the virtual machine should be in.</div>
                        <div>If <code>state</code> is set to <code>present</code> and the virtual machine exists its configuration will be updated to match that provided.</div>
                        <div>If <code>state</code> is set to <code>present</code> and the virtual machine does not exist it will be created. In this mode the power state will be set to off. If a VM with the same name already exists the VM creation will fail.</div>
                        <div>If <code>state</code> is set to <code>absent</code> and the virtual machine exists it will be deleted. Any snapshots of this VM will be deleted. Any attached volume groups will be detached.</div>
                        <div>If <code>state</code> is set to one of the following <code>on</code>, <code>off</code>, <code>powercycle</code> and the virtual machine does not exists, it will be created with the given parameters and the power state set.</div>
                        <div>If <code>state</code> is set to <code>on</code> and the virtual machine exists the power state will be powered on.</div>
                        <div>If <code>state</code> is set to <code>off</code> and the virtual machine exists the power state will be powered off.</div>
                        <div>If <code>state</code> is set to <code>reset</code> and the virtual machine exists the VM will be hard reset.</div>
                        <div>If <code>state</code> is set to <code>powercycle</code> and the virtual machine exists the VM will be powered off then back on.</div>
                        <div>If <code>state</code> is set to <code>guest-reboot</code> and the virtual machine exists the VM will be soft rebooted.</div>
                        <div>If <code>state</code> is set to <code>guest-shutdown</code> and the virtual machine exists the VM will be soft shutdown.</div>
                        <div>If <code>state</code> is set to <code>resume</code> and the virtual machine exists the VM will be resumed.</div>
                        <div>If <code>state</code> is set to <code>suspend</code> and the virtual machine exists the VM will be suspended.</div>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <div class="ansibleOptionAnchor" id="parameter-"></div>
                    <b>sysprep</b>
                    <a class="ansibleOptionLink" href="#parameter-" title="Permalink to this option"></a>
                    <div style="font-size: small">
                        <span style="color: purple">-</span>
                    </div>
                </td>
                <td>
                </td>
                <td>
                        <div>The sysprep content in XML format.</div>
                        <div>Should only be provided for windows systems being deployed.</div>
                        <div>Only applies to VM creation tasks.</div>
                        <div>Is mutually exclusive with <code>cloudinit</code></div>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <div class="ansibleOptionAnchor" id="parameter-"></div>
                    <b>timezone</b>
                    <a class="ansibleOptionLink" href="#parameter-" title="Permalink to this option"></a>
                    <div style="font-size: small">
                        <span style="color: purple">-</span>
                    </div>
                </td>
                <td>
                        <ul style="margin: 0; padding: 0"><b>Choices:</b>
                                    <li><div style="color: blue"><b>utc</b>&nbsp;&larr;</div></li>
                        </ul>
                </td>
                <td>
                        <div>The timezone for the virtual machine.</div>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <div class="ansibleOptionAnchor" id="parameter-"></div>
                    <b>username</b>
                    <a class="ansibleOptionLink" href="#parameter-" title="Permalink to this option"></a>
                    <div style="font-size: small">
                        <span style="color: purple">string</span>
                    </div>
                </td>
                <td>
                </td>
                <td>
                        <div>The username for either Nutanix Prism Element or Nutanix Prism Central for authentication.</div>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <div class="ansibleOptionAnchor" id="parameter-"></div>
                    <b>validate_certs</b>
                    <a class="ansibleOptionLink" href="#parameter-" title="Permalink to this option"></a>
                    <div style="font-size: small">
                        <span style="color: purple">string</span>
                    </div>
                </td>
                <td>
                        <ul style="margin: 0; padding: 0"><b>Choices:</b>
                                    <li><div style="color: blue"><b>yes</b>&nbsp;&larr;</div></li>
                                    <li>no</li>
                        </ul>
                </td>
                <td>
                </td>
            </tr>
    </table>
    <br/>


Notes
-----

.. note::
   - Check mode is supported.
   - Ensure that the user account used to connect to Prism Element or Prism Central has the appropriate rights to create and manage virtual machines.
   - If the change being requested is disruptive (such as changing virtual CPU) it will fail unless the force parameter is set to True. In this case the virtual machine will be powered off for the change then returned to its original power state.
   - Although VM names do not have on each Nutanix cluster, this module assumes that within each individual cluster there is only one instance of each virtual machine name.







Status
------


Authors
~~~~~~~

- Ross Davies <davies.ross@gmail.com>
